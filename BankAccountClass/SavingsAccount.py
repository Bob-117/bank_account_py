"""Classe COMPTE EPARGNE"""
from BankAccountClass import Account


class SavingsAccount(Account):
    """Classe COMPTE EPARGNE avec interets"""

    def __init__(self,
                 account_number: int,
                 owner_name: str,
                 balance: round(2),
                 bank_interest_rate: int):
        """
        Constructeur : Instanciation d'un objet de classe SavingsAccount heritant de Account
        :param numero_de_compte, nom, solde, interets:
        :return Account object(numero_de_compte, nom, solde, interets):
        """

        # print("test init checking account")
        super().__init__(account_number, owner_name, balance)
        # FIXME : new_bank_interest_rate = bank_interest_rate if 0 < bank_interest_rate < 1
        #  else bank_interest_rate * 100 if 0<x<100
        self.__bank_interest_rate = bank_interest_rate

    # ################### DEBUG #####################

    def __str__(self):
        return "Je suis une instance de la classe CheckingAccount"

    def show(self):
        """
        Surcharge de la methode show en print
        """
        super(SavingsAccount, self).show()
        print('------', 'interet', self.get_bank_interest_rate(), '%')
        print('******************')

    # ################### GETTER ####################

    def get_bank_interest_rate(self):
        """GETTER attribut bank_charges, taux d'agios du compte"""
        return self.__bank_interest_rate

    # #################### SETTER ####################

    def set_bank_interest_rate(self, new_bank_interest_rate):
        """SETTER attribut overdraft, autorisation de decouvert du compte"""
        self.__bank_interest_rate = new_bank_interest_rate

        # ################### TO DO ###################

    def withdraw(self, funds):
        """
        Retrait d'un montant 'funds' du compte instancie self
        Met a jour le solde du compte
        methode 'retrait' du cahier des charges
        Surcharge de la methode de la class mere :
        un compte epargne ne peut pas avoir un solde negatif
        """
        new_balance = self.get_balance() - int(funds)
        return self.set_balance(new_balance) if self.allowed_withdraw(funds) else False

    def allowed_withdraw(self, funds):
        """Return True ou False si le retrait est autorise"""
        new_balance = self.get_balance() - int(funds)
        return new_balance >= 0

    def deposit(self, funds):
        """
        Override deposit(), on touche des interets sur un compte epargne
        """
        return self.set_balance(self.apply_bank_interest_rate(funds))

    def apply_bank_interest_rate(self, funds):
        """"
        Appliquer les interet sur un depot d'argent sur un comtpe epargne
        On considere qu'a chaque versement de X sur un compte epargne,
        on touche des interet sur solde + X
        methode 'appliquer_interet' du cahier des charges
        :param funds:
        :return interest = funds * bank_interest_rate:
        """
        # FIXME : on touche des interets exponentiels sur la somme versee,
        #  ce qui pousse a faire des gros depots d'un coup
        return self.new_balance_with_interest(funds)

    def new_balance_with_interest(self, funds):
        """Cacul du nouveau solde apres interets"""
        # print((self.get_balance() + funds))
        # print(1 + self.get_bank_interest_rate() / 100)
        return round(
            (self.get_balance() + int(funds)) * (1 + self.get_bank_interest_rate() / 100)
            , 2
        )

    def send_to_shell(self, option):
        """
        Envoi des attributs du Backend vers le Frontend

        """
        match option:
            case 'proprietaire':
                return self.get_name()
            case 'numero de compte':
                return self.get_account_number()
            case 'solde':
                return self.get_balance_shell()
            case 'interets':
                return self.get_bank_interest_rate()
