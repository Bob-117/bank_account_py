"""Classe COMPTE COURANT"""
from BankAccountClass import Account


class CheckingAccount(Account):
    """Classe COMPTE COURANT avec decouvert et agios"""
    def __init__(self,
                 account_number: int,
                 owner_name: str,
                 balance: round(2),
                 overdraft: int,
                 bank_charges: int):
        """
        Constructeur : Instanciation d'un objet de classe CheckingAccount heritant de Account
        :param numero_de_compte, nom, solde, decouvert, agios:
        :return Account object(numero_de_compte, nom, solde, decouvert, agios):
        """
        super().__init__(account_number, owner_name, balance)
        self.__overdraft = abs(overdraft) * -1
        self.__bank_charges = bank_charges

    # def __init__(self, **kwargs):
    #     """
    #    Constructeur : Instanciation d'un objet de classe CheckingAccount heritant de Account
    #    :param numero_de_compte, nom, solde, decouvert, agios:
    #    :return Account object(numero_de_compte, nom, solde, decouvert, agios):
    #    """
    #     valid_keys = ["account_number", "owner_name", "balance", "overdraft", "bank_charges"]
    #     for key in valid_keys:
    #         setattr(self, key, kwargs.get(key))

    # ################### DEBUG #####################

    def __str__(self):
        return "Je suis une instance de la classe CheckingAccount"

    def show(self):
        """
        Surcharge de la methode show en print
        """
        super().show()
        print('------', 'decouvert :', self.get_overdraft())
        print('------', 'agios', self.get_bank_charges(), '%')
        print('******************')

    # ################### GETTER ####################

    def get_overdraft(self):
        """GETTER attribut overdraft, autorisation de decouvert du compte"""
        return self.__overdraft

    def get_bank_charges(self):
        """GETTER attribut bank_charges, taux d'agios du compte"""
        return self.__bank_charges

    # #################### SETTER ####################

    def set_overdraft(self, new_overdraft):
        """SETTER attribut overdraft, autorisation de decouvert du compte"""
        self.__overdraft = new_overdraft

    def set_bank_charges(self, new_bank_charges):
        """SETTER attribut bank_charges, taux d'agios du compte"""
        self.__overdraft = new_bank_charges

    # ################### TO DO ###################

    def apply_bank_charges(self, funds):
        """"
            Appliquer les agios sur un retrait d'argent d'un compte courant
            methode 'appliquer_agios' du cahier des charges
            On considere un retrait, solde et nouveau_solde
                Si solde > 0 && nouveau_solde > 0
                    => pas d'agios
                Si decouvert_autorise < nouveau_solde < 0
                    => paiement d'agios en % du retrait, dans la limite du decouvert autorise
                    ( abs(nouveau solde) + abs(agios) < abs(decouvert_autorise)
                Si nouveau_solde < decouvert autorise
                    => pas de retrait possible
            Cette methode appelera 2 methodes de verification :
                1) verifier le format des agios (0.1 = 110 = 10%)
                2) verifier qu'un potentiel retrait + agios ne depasse pas le decouvert autorise
            methode 'appliquer_agios' du cahier des charges
            :param self, funds:
            :return interest = funds * bank_interest_rate:
            """
        return int(funds) * (self.get_bank_charges() / 100)

    # ################### OTHER ###################

    def withdraw(self, funds):
        """
        Retrait d'un montant 'funds' du compte instancie self
        Met a jour le solde du compte
        methode 'retrait' du cahier des charges
        Surcharge de la methode de la class mere :
        un compte courant a une limite de decouvert autorise + agios potentiels
        On considere les var solde_actuel et nouveau_solde
        On traite les cas suivants :
            solde_actuel > 0 AND nouveau_solde > 0 => retrait de X
            solde_actuel > 0 AND decouvert < nouveau_solde < 0 => retrait de X + agios
            solde actuel < decouvert < 0 OR nouveau_solde < decouvert => retrait de X impossible
        """
        # print('withdraw checking')
        # print('t1', self.get_balance())
        if self.allowed_withdraw(funds):
            # print('allowed')
            new_balance = self.new_balance_with_charges(funds) \
                if self.get_balance() - int(funds) < 0 \
                else self.get_balance() - int(funds)
            return False if new_balance < self.get_overdraft() else self.set_balance(new_balance)
        else:
            return False
        # print('t2', new_balance)
        # print(self.get_overdraft())
        # print(self.set_balance(new_balance) if new_balance > self.get_overdraft() else False)
        # return False if new_balance < self.get_overdraft() else self.set_balance(new_balance)

    def allowed_withdraw(self, funds):
        """
        retrait si et seulement si solde > overdraft
        """
        return self.get_balance() - (funds + self.apply_bank_charges(funds)) > self.__overdraft

    def new_balance_with_charges(self, funds):
        """
        new solde si agios appliques
        """
        return round((self.get_balance() - (int(funds) + self.apply_bank_charges(int(funds)))), 3)

    def send_to_shell(self, option):
        """
        Envoi des attributs du Backend vers le Frontend

        """
        match option:
            case 'proprietaire':
                return self.get_name()
            case 'numero de compte':
                return self.get_account_number()
            case 'solde':
                return self.get_balance_shell()
            case 'decouvert':
                return abs(self.get_overdraft()) * -1
            case 'agios':
                return self.get_bank_charges()
