"""Classe abstraite COMPTE BANCAIRE"""
from abc import ABC


class Account(ABC):
    """Classe abstraite COMPTE BANCAIRE"""

    def __init__(self, account_number: int, owner_name: str, balance: float):
        """
        Constructeur : Instanciation d'un objet de classe Account, classe abstraite
        :param numero_de_compte, nom, solde:
        :return Account object(numero_de_compte, nom, solde):
        """
        # print("test init account")
        self.__account_number = account_number
        self.__owner_name = owner_name
        self.__balance = balance

    # ################### GETTER ###################

    def get_name(self):
        """GETTER attribut owner_name, nom du proprietaire du compte"""
        return self.__owner_name

    def get_account_number(self):
        """GETTER attribut account_number, numero du compte"""
        return self.__account_number

    def get_balance(self):
        """GETTER attribut balance, solde du compte"""
        return self.__balance

    def get_balance_shell(self):
        """GETTER attribut balance, solde du compte"""
        return round(self.__balance, 2)

    # ################### SETTER ###################

    def set_name(self, new_name):
        """SETTER attribut owner_name, nom du proprietaire du compte"""
        self.__owner_name = new_name

    def set_account_number(self, new_account_number):
        """SETTER attribut account_number, numero du compte"""
        self.__account_number = new_account_number

    def set_balance(self, new_balance):
        """SETTER attribut balance, solde du compte"""
        self.__balance = new_balance
        return True

    # ################### DEBUG ###################

    def __str__(self):
        return print("Je suis une instance de la classe abstraite Account")

    def get_all(self):
        # TODO creer le dict dynamiquement
        """Return un dict {nom de l'attribut : valeur de l'attribut} d'un objet Compte"""

        # dict = {}
        # for attribut in vars(self):
        # dict[attribut.name] = "self.attribut"
        # return dict

        # return self.__dict__

        return {'Compte numero :': self.get_account_number(),
                'Proprietaire : ': self.get_name(),
                'Solde : ': self.get_balance()
                }

    def get_all_json(self):
        """
        Renvoie un objet json de notre compte instancie
        :param Account object:
        :return json object:
        """
        # dict = {}
        json_account = {}
        for key in vars(self):
            value = vars(self)[key]
            match key:
                case '_Account__account_number':
                    key = "account_number"
                case '_Account__owner_name':
                    key = "name"
                case '_Account__balance':
                    key = "balance"
                case '_SavingsAccount__bank_interest_rate':
                    key = "interest"
                case '_CheckingAccount__overdraft':
                    key = "overdraft"
                case '_CheckingAccount__bank_charges':
                    key = "charges"
            # key = "\"" + str(key) + "\""
            # value = "\"" + str(value) + "\""
            # key = str(key).replace("\"", "\'")
            json_account[key] = value
            # print(key, '->', value)
        # json_output = json.dumps(json_account)
        # print("output : ", json_output)
        # print(type(json_output))
        # print("JSON : ", json_output)
        # print(json.dumps(json_account, indent=4))
        # return json_output
        return json_account
        # for attribut in vars(self):
        #    dict[attribut.name] = "self.attribut"
        # return dict

    def show(self):
        """Affiche de facon lisisble les attributs d'un objet en console"""
        print("******************")
        match self.__class__.__name__:
            case 'Account':
                print("Compte -classe abstraite-")
            case 'CheckingAccount':
                print("Compte courant")
            case 'SavingsAccount':
                print("Compte epargne")
        for key, value in self.get_all().items():
            # if type(value) == int:
            # value_shell = round(value, 2)
            # if value == str and value.isnumeric:
            # value_shell = round(value, 2)
            print('------', key, value)
        # print("******************")

    # ################### TO DO ###################

    def withdraw(self, funds):
        """
        Retrait d'un montant 'funds' du compte instancie self
        Met a jour le solde du compte
        methode 'retrait' du cahier des charges
        """
        # print("retrait de ", funds, '€')
        print('withdraw account')
        new_balance = self.get_balance() - int(funds)
        self.set_balance(new_balance)

    def allowed_withdraw(self, funds):
        """Interface method allwoed_withraw()"""

    def deposit(self, funds):
        """
        Depot d'un montant 'funds' sur le compte instancie self
        Met a jour le solde du compte
        methode 'depot' du cahier des charges
        """
        # print("depot de ", funds, '€')
        # print("________________________________________")
        # print("funds : ", funds)
        # print("typefunds : ", type(funds))
        # print("balance : ", self.get_balance())
        # print("typebalance : ", type(self.get_balance()))
        # print("________________________________________")
        new_balance = self.get_balance() + float(funds)
        self.set_balance(new_balance)

    def show_balance(self):
        """
        Affichage du solde du compte instancie
        methode 'afficher_solde' du cahier des charges [ print(account.show_balance()) ]
        :return self.balance:
        """
        return self.get_balance()

    def send_to_shell(self, option):
        """
        Envoi des attributs du Backend vers le Frontend

        """
        match option:
            case 'proprietaire':
                return self.get_name()
            case 'numero de compte':
                return self.get_account_number()
            case 'solde':
                return self.get_balance_shell()
