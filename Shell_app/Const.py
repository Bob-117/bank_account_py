"""Const module"""
import os

## USER INPUT

#language menu
lang = {
    'EN': 'en',
    'FR': 'fr'
}
# Menu
actions_fr = {
    'Consulter un compte/Effectuer une action': 'view',
    'Creer un compte': 'creer',
    'Quitter': 'exit'
}
actions_en = {
    'View an account/Operations on account': 'view',
    'Createan account': 'creer',
    'Exit app': 'exit'
}

# Menu 1
account_actions = {
    'Consulter': 'view',
    'Retrait': 'retrait',
    'Depot': 'depot',
    'Menu': 'menu'
}

account_print = {
    'Imprimer': 'print',
    'Menu': 'menu'
}

# Menu 2 = create
account_type = {
    'Creer un compte courant': 'courant',
    'Creer un compte epargne': 'epargne',
    'Retour au menu principal': 'menu'
}



account_deposit = {
    'Entrer un montant du depot': 'depot',
    'Annuler': 'menu'
}

account_withdraw = {
    'Entrer un montant du retrait': 'retrait',
    'Annuler': 'menu'
}

# SHELL VIEW

CheckingAccountView = [
    'proprietaire',
    'numero de compte',
    'solde',
    'decouvert',
    'agios'
]

SavingsAccountView = [
    'proprietaire',
    'numero de compte',
    'solde',
    'interets'
]

# FAKE DATABASE
DB = {
    '1': 'un',
    '2': 'deux',
    '3': 'trois'
}

ROOT_DIR1 = os.path.dirname(os.path.abspath(__file__)) # This is your Project Root
ROOT_DIR2 = os.path.abspath(os.curdir)

# CHOIX DYNAMIQUE DE LANGUE POUR LES MENUS / L'APPLICATION SHELL
LANGUAGE_EN = {
    'menu1':
        {
            'action1': 'deposit',
            'action2': 'withdraw'
        },
    'menu2':
        {
            'action1': 'create',
            'action2': 'delete'
        }
}

LANGUAGE_FR = {
    'menu1':
        {
            'action1': 'virement',
            'action2': 'retrait'
        },
    'menu2':
        {
            'action1': 'creer',
            'action2': 'supprimer'
        }
}
