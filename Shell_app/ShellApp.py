"""ShellApp module"""
import os
import sys
import copy

# Backend import
from BankAccountClass import CheckingAccount as Checking
from BankAccountClass import SavingsAccount as Savings

# Frontend import
from Shell_app import Const
from Shell_app import user_is_trolling as troll

# DB import
from DB_Json import DataBaseJson as DB

# Security import


# tools import (shell clear, loading screen and more)
from Tools import loading
from Tools import tool_print_shell


class ShellApp:
    """Main shell app, run the app"""
    def __init__(self, admin, data_base, language):
        """Shell app constructor, with one admin and one database"""
        # print('ShellApp init')
        self.__admin = admin
        self.__db = DB(data_base, '')
        self.__language = language
        # print("Const.actions_" + self.__language)
        self.__language_data = "Const.actions_" + self.__language
        # print(language_data)
        print(type(Const))

    def clear_shell(self, pwd):
        """clear shell between 2 menus"""
        # print("clear")
        # print(os.name)
        os.system('cls' if os.name == 'nt' else 'clear')
        # print("\n" * 20)
        print('=== Session Admin : ', self.__admin, '===')
        print(pwd)

    def user_is_asking(self):
        """
        test
        """
        test = input('test?')
        print(self.__admin, 'is saying', test)

    def main_user_prompt(self):
        """
        afficher les differents menus en fonction de la commande de l'utilisateur
        => ajouter konami code
        """

        current_path = 'Application Bancaire / '
        self.clear_shell(current_path)
        # FIXME : connexion possible ? seul un admin peut CRUD, sinon only read
        match self.__language:
            case 'en':
                user_input = self.select_user_action(
                    Const.actions_en,
                    'Your choice',
                    current_path, True)
            case 'fr':
                user_input = self.select_user_action(
                    Const.actions_fr,
                    'Votre choix',
                    current_path, True)

        # constfile = "Const.actions" + self.__language
        # user_input = self.select_user_action(constfile, 'Your choice', current_path, True)

        match user_input:
            case 'creer':
                self.create_input(current_path, user_input)
            case 'view':
                self.view('')
            case 'exit':
                sys.exit()
            case '_':
                print('aucun choix')

    def select_user_action(self, options, name, current_path, clear):
        """
       Select user menu input
       """

        if clear:
            self.clear_shell(current_path)

        selected = None
        index = 0
        choice_list = []
        print('--------------------')
        print('Faites votre choix :')

        for option_name in options:
            index = index + 1
            choice_list.extend([options[option_name]])
            print(str(index) + ') ' + option_name)
        input_is_valid = False

        while not input_is_valid:

            user_input = (input(name + ': '))
            if user_input.isnumeric() and 0 < int(user_input) < len(choice_list) + 1:
                selected = choice_list[int(user_input) - 1]
                # print('Selected ' + name + ': ' + selected)
                input_is_valid = True
                break
            else:
                print('Selectionnez une action valide !')

        return selected

    def account_number_is_int(self, account_number):
        """check withdraw/deposit user input type"""
        while not (account_number.isnumeric() or isinstance(account_number, int)):
            account_number = input('Entrez un vrai numero de compte (q = quit): ')
            if account_number == "q":
                self.main_user_prompt()
        return int(account_number)

    def create_input(self, current_path, user_input):
        """
        Main account creation method
        """
        current_path += 'Creation / '

        # selection du type de compte a creer
        user_action = self.select_user_action(
            Const.account_type,
            'Type de compte a creer',
            current_path, True)

        # creation du nouveau compte = CheckingAccount ou SavingsAccount
        new_account = self.create(user_input)

        # on enregistre le nouveau compte dans la db
        self.__db.save_one_account_to_bd_json(new_account.get_all_json())
        self.main_user_prompt()

    def view(self, current_path):
        current_path += 'Consultation / '

        # entree utilisateur d'un numero de compte a consulter
        account_number = input('Entrez une numero de compte : ')
        account_number = self.account_number_is_int(account_number)

        # SELECT account FROM database WHERE number = account_number
        selected_account = self.__db.select_one_account_number_AccountObject(account_number)

        # Si le compte n'existe pas, retour au menu principal
        if not selected_account:
            loading("Pas de correspondance" + "\n" + "Retour au menu principal", 0.2)
            self.main_user_prompt()
        # Si le compte existe et a ete selectionne
        else:
            current_path += 'Compte numero '
            current_path += str(selected_account.get_account_number())
            current_path += ' / '

            # Menu consulter un compte, retrait ou depot
            user_action = self.select_user_action(Const.account_actions,
                                                  'Choix',
                                                  current_path, True)
            match user_action:
                case 'view':
                    self.main_details(current_path, selected_account)
                case 'retrait':
                    self.main_withdraw(current_path, account_number, selected_account)
                case 'depot':
                    self.main_deposit(current_path, account_number, selected_account)
                case 'menu':
                    self.main_user_prompt()
    def create(self, user_input):
        """
        Account creation menu, checking or savings
        """
        # print('Creation sous la session admin : ', self.__admin)
        print('Creation d\'un compte ', user_input, 'sous la session admin : ', self.__admin)
        # print("Vous voulez " + user_action + " un compte " + user_input)
        new_account = None
        match user_input:
            case 'courant':
                new_account = self.create_account('courant')
            case 'epargne':
                new_account = self.create_account('epargne')
        return new_account

    def create_account(self, account_type):
        """
        Creation d'un compte cote Frontend avec des entrees utilisateur en shell
        Verification de l'unicite du nouveau numero de compte
        Verification du type numeric du nouveau numero de compte
        :params self, account_type:
        :return Account object with user input attributes:
        """

        new_number_account = input('Entrez un numero de compte : ')

        numeric_exist = self.is_unique_and_numeric(new_number_account)

        while not numeric_exist['numeric'] or not numeric_exist['exist']:
            if not numeric_exist['numeric']:
                new_number_account = input('Entrez un vrai numero de compte '
                                           '(q = quit): ')
                numeric_exist = self.is_unique_and_numeric(new_number_account)
            elif not numeric_exist['exist']:
                new_number_account = input('Numero de compte existant, entrez un nouveau '
                                           'numero de compte (q = quit): ')
                numeric_exist = self.is_unique_and_numeric(new_number_account)

        new_name = input('Entrez un nom : ')
        new_balance = 0
        match account_type:
            case 'courant':
                new_overdraft = 100
                new_bank_charges = 10
                return Checking(int(new_number_account),
                                str(new_name),
                                new_balance,
                                new_overdraft,
                                new_bank_charges)
            case 'epargne':
                new_bank_interest_rate = 10
                return Savings(int(new_number_account),
                               str(new_name),
                               new_balance,
                               new_bank_interest_rate)

    def is_unique_and_numeric(self, number):
        """check if new account number is numeric and unique in db"""
        if number == "q":
            self.main_user_prompt()
        is_numeric = not troll.trolling_user_menu(number, 'numeric')
        is_unique = not self.__db.select_one_account_number(number)
        return {'numeric': is_numeric, 'exist': is_unique}

    def show_in_shell(self, account):
        """show an account in shell, ux/ui"""
        account_type = account.__class__.__name__ + 'View'
        keys = None
        match account_type:
            case 'CheckingAccountView':
                keys = Const.CheckingAccountView
            case 'SavingsAccountView':
                keys = Const.SavingsAccountView
        print('*********************')
        for key in keys:
            print(key, ' : ', account.send_to_shell(key))
        print('*********************')

    def main_details(self, current_path, selected_account):
        """Main show account details"""
        current_path += 'Consultation d\'un compte'
        print(current_path)
        # Afficher le compte selectionne
        self.show_in_shell(selected_account)
        user_action = self.select_user_action(Const.account_print,
                                              'Choix',
                                              current_path,
                                              False)
        match user_action:
            case 'print':
                tool_print_shell(selected_account)
                self.main_user_prompt()
            case 'menu':
                self.main_user_prompt()

    def main_withdraw(self, current_path, account_number, selected_account):
        """Main withdraw method"""
        current_path += 'Retrait / '
        print(current_path)
        user_action = self.select_user_action(Const.account_withdraw,
                                              'Choix',
                                              current_path,
                                              True)
        match user_action:
            case 'retrait':
                withdraw_funds = input('Entrez montant de retrait : ')
                withdraw_funds = self.withdraw_deposit_is_int(withdraw_funds)
                # depot sur le selected account
                if selected_account.allowed_withdraw(withdraw_funds):
                    selected_account.withdraw(withdraw_funds)

                    # copie de compte (utilite?)
                    updated_account = copy.deepcopy(selected_account)

                    #  updated account en json pour update en db
                    updated_account_to_save = updated_account.get_all_json()

                    # UPDATE accounts
                    # SET balance
                    # WHERE account_number = selected_number
                    self.__db.update_one_account_to_bd_json(
                        updated_account_to_save,
                        account_number
                    )
                    load_message_array = [
                        "Retrait de : ",
                        str(withdraw_funds),
                        "€, nouveau solde : ",
                        "%.2f" % selected_account.get_balance(),
                        "€"
                    ]
                    if isinstance(selected_account, Checking) and \
                            selected_account.get_overdraft() < selected_account.get_balance() < 0:
                        agios = str(selected_account.apply_bank_charges(withdraw_funds))
                        checking_message_array = ["\ninteret : ",
                                                  str(selected_account.get_bank_charges()), "%, ",
                                                  "agios percus : ",
                                                  "%.2f" % float(agios), "€"]
                        load_message_array += checking_message_array

                    load_message = "".join(load_message_array)
                    loading(load_message, 0.4)
                    input("Press any key to continue...")
                    self.main_user_prompt()
                else:
                    # Si retrait impossible
                    self.cant_withdraw_message(selected_account)

            case 'menu':
                self.main_user_prompt()
            case '_':
                print('aucun choix')

    def withdraw_deposit_is_int(self, funds):
        """check withdraw/deposit user input type"""
        while not (funds.isnumeric() or isinstance(funds, int)):

            funds = input('Entrez un vrai montant (q = quit): ')
            if funds == "q":
                self.main_user_prompt()

        return int(funds)

    def cant_withdraw_message(self, selected_account):
        """
        Show why u cant withdraw
        """
        load_message = ""
        load_message_cant_withdraw = ["Solde insuffisant, "]
        if isinstance(selected_account, Checking):
            load_message = ['decouvert : ',
                            str(selected_account.send_to_shell('decouvert')),
                            '\nRetour au menu']
        if isinstance(selected_account, Savings):
            load_message = ['Vous ne pouvez pas etre en negatif sur un compte epargne',
                            '\nRetour au menu']
        load_message_cant_withdraw += load_message
        loading("".join(load_message_cant_withdraw), 0.5)
        self.main_user_prompt()

    def main_deposit(self, current_path, account_number, selected_account):
        """Main deposit method"""
        current_path += 'Depot / '
        user_action = self.select_user_action(Const.account_deposit,
                                              'Choix',
                                              current_path,
                                              True)
        match user_action:
            case 'depot':
                deposit_funds = input('Entrez montant de depot : ')
                deposit_funds = self.withdraw_deposit_is_int(deposit_funds)
                # depot sur le selected account
                selected_account.deposit(deposit_funds)

                # copie de compte (utilite?)
                updated_account = copy.deepcopy(selected_account)

                #  updated account en json pour update en db
                updated_account_to_save = updated_account.get_all_json()

                # UPDATE accounts SET balance WHERE account_number = selected_number
                self.__db.update_one_account_to_bd_json(
                    updated_account_to_save,
                    account_number
                )
                self.main_user_prompt()
            case 'menu':
                self.main_user_prompt()
            case '_':
                print('aucun choix')
