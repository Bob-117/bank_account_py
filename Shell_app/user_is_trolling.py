"""user is trolling us, everywhere, everytime"""
def trolling_user_menu(value, option):
    """
    verifier si l'entree utilisateur est valide
    lettres pour le numero de compte
    test sur les % agios et interet (0<x<1 ou 0<x<100)
    """
    match option:
        case 'numeric':
            # print('numeric')
            return not value.isnumeric() or ''
        case 'str':
            return isinstance(value, str) or value == ''
        # case 'rate':
        #     return value.isnumeric \
        #            and 0 < value < 100 \
        #            or re.match(value, '0.$$')   regex pourcentage ????
