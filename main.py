# -*- coding: utf-8 -*-
"""Run main to start my lovely bank app"""

from Shell_app.ShellApp import ShellApp as Shell
from Shell_app.ShellApp import Const


def select_language(options, name):
    """
    Select user menu input
    """
    selected = None
    index = 0
    choice_list = []
    print('--------------------')
    print('Faites votre choix :')

    for option_name in options:
        index = index + 1
        choice_list.extend([options[option_name]])
        print(str(index) + ') ' + option_name)
    input_is_valid = False

    while not input_is_valid:

        user_input = (input(name + ': '))
        if user_input.isnumeric() and 0 < int(user_input) < len(choice_list) + 1:
            selected = choice_list[int(user_input) - 1]
            # print('Selected ' + name + ': ' + selected)
            input_is_valid = True
            break
        else:
            print('Selectionnez une action valide !')

    return selected


# def select_language_arrow():
#     options = ['EN', 'FR']
#     choice = enquiries.choose('Choose one of these options: ', options)
#     print(choice)


if __name__ == '__main__':
    # os.system('mode con: cols=80 lines=30')

    # kernel32 = ctypes.WinDLL('kernel32')
    # user32 = ctypes.WinDLL('user32')
    #
    # SW_MAXIMIZE = 1
    #
    # hWnd = kernel32.GetConsoleWindow()
    # user32.ShowWindow(hWnd, SW_MAXIMIZE)
    # tool_clear_shell()

    input("Press any key to continue...")
    # select_language_arrow()
    language = select_language(Const.lang, "Pick a language")
    shell = Shell('bob', 'DB_Json/db.json', language)

    shell.main_user_prompt()
