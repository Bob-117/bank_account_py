# POEC CYBERSECURITE 2022 - EPSI Rennes

### 24 mars 2022 / 26 juin 2022

![Alt text](readme_svg/made-with-python.svg)

![Alt text](readme_svg/uses-pylint.svg) ![Alt text](readme_svg/uses-pytest.svg)



## Exercice d'entrainement au developpement Python
&rarr; Simulation de comptes bancaires et de differentes transaction

https://gitlab.com/docusland/python-comptes
### TODO
- [x] Initialisation git
- [x] Une classe mere "Compte"
  - [x] attributs : numeroCompte, nomProprietaire, solde
  - [x] methodes : retrait, versement, afficherSolde
- [x] Une classe fille "Compte Courant"
  - [x] attributs : autorisationDecouvert, pourcentageAgios
  - [x] methodes : appliquerAgios
- [x] Une classe fille "Compte Epargne"
  - [x] attributs : pourcentageInterets
  - [x] methodes : appliquerInterets
- [X] Une interface shell
- [ ] Une interface graphique (&rarr; TK Library... pygame ?)

## Pour l'evaluation : 

### Les méthodes demandées dans le cahier des charges dans la catégorie  ' #### TODO #####' des différentes classes
### Lancer le fichier main.py pour lancer l'interface graphique (en travaux)
### Lancer test.py pour tester les differentes methodes (sans print)
### Un avis sur le bordel DataBase et le Shell_app est le bienvenu


# Versions :
## 1.0.0 commit sha : 840169d7027b09b44284fca054fb95c127ab3ff8
  - Classes Compte, Compte Courant et Compte Epargne
  - Methodes demandées pour l'évaluation
  - Mise en place de l'arboresence database (simulée par un fichier json pour commencer)
  - Mise en place de l'arboresence ShellApp (interface utilisateur en ligne de commande)

## 2.0.0 commit sha : a134bb2e20a3b881045234be6b8e5aa3e65232e5
  - Interface shell 
    - Lecture de db.json (select all, select one)
    - Enregistrement d'un nouveau compte cree en shell dans db.json
    - Ajout de sécurité (if account != null etc)

## 3.0.0 commit sha : 492a6e70bf19444f3ce7d8544f745682862e1010
  - Shell interface 
    - Create, Read, Update account in json database
    - Create, deposit, withdraw from shell
    - Back-End security (UnitTest with pyTest)
    - Back-End security (Clean code with pyLint)
    - Back-End security (case sensitivity)
    - Front-End security (new account number *is_unique()* + *is_numeric()*...)
    - Front-End UX/UI Menu (autorun, exit, q = quit)
## 3.5.0 commit sha : 1e5aab7f73418773d7f7cd4bea4fff3fd136c4e2
```shell
path\Bank_Account> pylint $(git ls-files '*.py')
...
Your code has been rated at 9.22/10 (previous run: 9.22/10, +0.00)
```

### TODO :
  - [X] ShellApp (+sécurité user_input frontend)
  - [X] Back-End security ( positive and negative numbers, %rates)
    => Need decorators
  - [ ] Database json CRUD
    - [X] INSERT INTO accounts VALUES new_account
    - [X] SELECT * FROM accounts
    - [X] SELECT acc FROM accounts WHERE number = 117
    - [X] UPDATE
    - [ ] DELETE
  - [ ] Database : mariadb / SQLite3
  - [X] Shell interface
  - [ ] PyGame Interface
  - [ ] TK Interface
  - [ ] CRUD Account with Admin Role
  - [ ] EN / FR version
