"""test module"""
from BankAccountClass import Account
from BankAccountClass import CheckingAccount as Checking
from BankAccountClass import SavingsAccount as Savings
from Shell_app.ShellApp import ShellApp as Shell
from DB_Json.DataBaseJson import DataBaseJson as DataBase

acc = Account(117, 'bob', 100)
che = Checking(117, 'bobby', 100, -100, 10)
sav = Savings(117, 'gibson', 100, 10)

def test_evaluation(account):
    """
    test general des differentes methodes
    """
    account.show()
    print('depot de 100')
    account.deposit(100)
    account.show()
    print('retrait de 100')
    account.withdraw(100)
    account.show()
    print('retrait de 150')
    account.withdraw(150)
    account.show()


def test_overdraft(checking):
    """
    fonction de test de retrait en prenant en compte le decouvert
    et les agios
    je suis plutot content du return ternaire
    """

    checking.show()
    checking.withdraw(182)
    checking.show()
    checking.withdraw(181)
    checking.show()


# test_overdraft(che)
# test_evaluation(acc)
# test_evaluation(che)
# test_evaluation(sav)

def test():
    """test1 method"""
    # acc = account(117, 'bob', 1000)
    # che = Checking(118, 'bobby', 100, -100, 10)
    # sav = Savings(119, 'gibson', 1000, 20)
    list_test = [acc, che, sav]
    for item in list_test:
        print(item.__class__.__name__)
    acc.show()
    acc.deposit(100)
    acc.show()
    acc.withdraw(50)
    acc.show()
    print(acc.show_balance())
    ################
    che.show()
    print((che.withdraw(50)))
    print((che.withdraw(181.9)))
    che.show()
    ################
    sav.show()
    sav.withdraw(989)
    sav.show()
    sav.withdraw(10)
    sav.show()
    sav.withdraw(1001)
    sav.show()
    sav.deposit(100)
    sav.show()
    ###############
    # shell = Shell('bob')
    # shell.user_is_asking()
    ############
    # db = DataBase('bdd.json', 'L')
    # db.save()
    ##############
    # shell = Shell('bob', db)
    # shell.main_user_prompt()
    # db.openfile()
    # test()

    # db = DataBase(C.ROOT_DIR2 + '/BDD/bdd.json', 'L')
    # db.save()
    # shell = Shell('bob', db)
    # db.openfile()
    # shell.main_user_prompt()

    # test_evaluation(acc)
    # test_evaluation(che)
    # test_evaluation(sav)

    # db = DataBase('/bdd.json', None)
    print('========================')
    # # print(db)
    # print('========================')
    # db.select_all()
    # db.select_one_id('ACCOUNTS', 0)
    # print('========================')
    # masterchief = db.select_one_account_number(117)
    # print(masterchief)
    # print('========================')
    # db = DataBase('/bdd.json', None)
    # sav.get_all_json()
    # db.save_one_account_to_db(che)
    # # db.select_all()
    # # db.save()

    # selected_id = db.select_one_id("ACCOUNTS", 1)
    # selected_number = db.select_one_account_number(117)
    # print(selected_id)
    # print(selected_number)

    db_test = DataBase('DB_Json/db.json', None)
    shell = Shell('bob', 'DB_Json/db.json')

    new_acc = Checking(314159265, 'bobby', 100, -100, 10)
    all_test = db.select_all('ADMIN')
    print(new_acc)
    print(all_test)
    selected_acc = db_test.select_one_account_number_AccountObject("118")
    shell.show_in_shell(selected_acc)
    # db.save_one_account_to_bd_json(new_acc.get_all_json())
    # db.reset_db()


db = DataBase('DB_Json/db.json', None)
db.reset_db()
a = db.select_one_account_number_AccountObject(11)
print(a)
