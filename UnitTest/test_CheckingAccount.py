"""CheckingAccount test module"""

import unittest

from BankAccountClass import CheckingAccount


# Tests adapted from `problem-specifications//canonical-data.json` @ v2.4.0


class CheckingAccountTest(unittest.TestCase):
    """Unit test : Checking Account class"""
    def test_deposit(self):
        """Can i deposit 100 on one Checking Account"""
        account = CheckingAccount(117, "bob", 100, 100, 10)
        account.deposit(100)
        self.assertEqual(account.get_balance(), 200)

    def test_allowed_withdraw_true(self):
        """critical case withdraw 181 is allowed"""
        account = CheckingAccount(117, "bob", 100, -100, 10)
        self.assertEqual(account.allowed_withdraw(181), True)

    def test_allowed_withdraw_false(self):
        """critical case withdraw 182 is not allowed"""
        account = CheckingAccount(117, "bob", 100, -100, 10)
        self.assertEqual(account.allowed_withdraw(182), False)

    def test_new_balance_with_charges_above_overdraft(self):
        """critical case withdraw 181 is allowed and compared with new_theoretical_balance"""
        account = CheckingAccount(117, "bob", 100, -100, 10)
        new_balance = account.new_balance_with_charges(181)
        self.assertEqual(new_balance, -99.1)

    def test_new_balance_with_charges_below_overdraft(self):
        """critical case withdraw 182 is not allowed but compared with new_theoretical_balance"""
        account = CheckingAccount(117, "bob", 100, -100, 10)
        new_balance = account.new_balance_with_charges(182)
        self.assertEqual(new_balance, -100.2)

    def test_withdraw_above_overdraft(self):
        """critical case withdraw 181 is allowed and applied"""
        account = CheckingAccount(117, "bob", 100, -100, 10)
        account.withdraw(181)
        self.assertEqual(account.get_balance(), -99.1)

    def test_withdraw_below_overdraft(self):
        """critical case withdraw 181 is not allowed and cant be applied"""
        account = CheckingAccount(117, "bob", 100, -100, 10)
        account.withdraw(182)
        self.assertEqual(account.get_balance(), 100)


if __name__ == '__main__':
    unittest.main()
