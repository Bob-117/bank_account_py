"""Account test module"""
import unittest

from BankAccountClass import Account


# Tests adapted from `problem-specifications//canonical-data.json` @ v2.4.0


class AccountTest(unittest.TestCase):

#  ################### GETTER TEST ###################
    def test_get_name(self):
        account = Account(117, "bob", 100)
        self.assertEqual(account.get_name(), "bob")

    def test_get_account_number(self):
        account = Account(117, "bob", 100)
        self.assertEqual(account.get_account_number(), 117)

    def test_get_balance(self):
        account = Account(117, "bob", 100)
        self.assertEqual(account.get_balance(), 100)

    def test_get_balance_shell(self):
        account = Account(117, "bob", 100.314159265)
        self.assertEqual(account.get_balance_shell(), 100.31)

# ################### SETTER TEST ###################

    def test_set_name(self):
        account = Account(117, "bob", 100)
        account.set_name("Gibson")
        self.assertEqual(account.get_name(), "Gibson")

    def test_set_account_number(self):
        account = Account(117, "bob", 100)
        account.set_account_number(31415)
        self.assertEqual(account.get_account_number(), 31415)

    def test_set_balance(self):
        account = Account(117, "bob", 100)
        account.set_balance(31415)
        self.assertEqual(account.get_balance(), 31415)

#  ################### METHODS TEST ###################
    def test_deposit(self):
        """Can i deposit 100 on one Account"""
        account = Account(117, "bob", 100)
        account.deposit(100)
        self.assertEqual(account.get_balance(), 200)

    def test_withdraw(self):
        """Can i withdraw 100 on one Account"""

        account = Account(117, "bob", 100)
        account.withdraw(100)
        self.assertEqual(account.get_balance(), 0)


if __name__ == '__main__':
    unittest.main()
