"""SavingsAccountTest module"""
import unittest

from BankAccountClass import SavingsAccount


# Tests adapted from `problem-specifications//canonical-data.json` @ v2.4.0


class SavingsAccountTest(unittest.TestCase):
    """Unit test : Savings Account class"""
    def test_deposit(self):
        """Can i deposit 100 on one Savings Account"""
        account = SavingsAccount(117, "bob", 100, 10)
        account.deposit(100)
        self.assertEqual(account.get_balance(), 220.0)

    def test_allowed_withdraw_true(self):
        """critical case withdraw 99 is allowed"""
        account = SavingsAccount(117, "bob", 100, 10)
        self.assertEqual(account.allowed_withdraw(99), True)

    def test_allowed_withdraw_false(self):
        """critical case withdraw 101 is not allowed"""
        account = SavingsAccount(117, "bob", 100, 10)
        self.assertEqual(account.allowed_withdraw(101), False)

    def test_withdraw(self):
        """critical case withdraw 17 is allowed and applied"""
        account = SavingsAccount(117, "bob", 100, 10)
        account.withdraw(17)
        self.assertEqual(account.get_balance(), 83)


if __name__ == '__main__':
    unittest.main()
