"""Classe DataBaseJson"""
import json

# Backend import
from BankAccountClass import CheckingAccount as Checking
from BankAccountClass import SavingsAccount as Savings

from Tools import loading


class DataBaseJson:
    """Classe DataBaseJson, pour simuler une db.json"""

    def __init__(self, path, data):
        self.path = path
        self.data = data
        self.indent = 4

    def __str__(self):
        return str('BDDpath :' + str(self.path) + ' --- data :' + str(self.data))

    def read_db(self):
        """
        Ouverture du fichier json simulant une bdd
        :params:
        :return db_full_content as dict:
        """

        with open(self.path, "r", encoding="utf-8") as file:
            return json.load(file)

    def select_all(self, table):
        """
        SELECT * FROM db, use self.read_db()
        :param:
        :return db_full_content STR json format:
        """
        data = None
        match table:
            case 'ACCOUNTS':
                data = self.read_db()['ACCOUNTS']
            case 'ADMIN':
                data = self.read_db()['ADMIN']
            case '*':
                data = self.read_db()
        return json.dumps(data, indent=self.indent)

    def select_one_id(self, table, index):
        """
        SELECT * FROM db WHERE id = id
        :params table id:
        :return db data = one account or admin with id selector:
        """
        data = self.read_db()
        return data[table][index + 1]

    def select_one_account_number(self, selected_number):
        """
        SELECT * FROM db WHERE number = 117, use self.read_db()
        :param selected_number:
        :return db data = one account with account_number selector:
        """
        data = self.read_db()['ACCOUNTS']
        selected_account = False
        for account in data:
            for key in account:
                if str(account[key]['account_number']) == str(selected_number):
                    selected_account = account[key]
        return selected_account if selected_account else {}

    def select_one_account_number_AccountObject(self, selected_number):
        """
        SELECT * FROM db WHERE number = 117, use self.read_db()
        :param selected_number:
        :return db data = one Account Object with account_number selector:
        """
        # print("select_one_account_number_AccountObject")
        account_dict = self.select_one_account_number(selected_number)
        # print(type(account_dict))
        # print(account_dict)
        selected_account = False
        if account_dict:
            if len(account_dict) == 4:
                number = account_dict["account_number"]
                name = account_dict["name"]
                balance = account_dict["balance"]
                interest = account_dict["interest"]
                selected_account = Savings(
                    number, name, balance, interest
                )
            if len(account_dict) == 5:
                selected_account = Checking(
                    account_dict["account_number"],
                    account_dict["name"],
                    account_dict["balance"],
                    account_dict["overdraft"],
                    account_dict["charges"]
                )
        return selected_account

    def save_one_account_to_bd_json(self, data):
        """
        INSERT INTO accounts VALUES accounts object
        :params data = one account:
        :return one saved-to-db account:
        """
        loading("Save", 0.2)
        # 1. Read file contents
        with open(self.path, "r", encoding="utf-8") as file:
            db_content = json.load(file)
        # 2. Update json object
        max_index = len(db_content['ACCOUNTS'])
        max_index_str = str(max_index + 1)
        auto_incr_index = f"{max_index_str}"

        new_dict = {auto_incr_index: data}
        db_content['ACCOUNTS'].append(new_dict)

        # 3. Write json file
        print(self.path)
        with open(self.path, "w", encoding="utf-8") as file:
            data_to_write = json.dumps(db_content, indent=self.indent)
            file.write(data_to_write)

    def update_one_account_to_bd_json(self, account_to_update, account_number):
        """
        UPDATE account
        SET balance = new_balance
        WHERE number = account_number
        """
        # Recuperer le contenu de la db
        db_content = self.open_db()
        # On parcourt la db
        index = 0
        while index < len(db_content['ACCOUNTS']):
            current_account = db_content['ACCOUNTS'][index]
            for keys in current_account:
                # On retient l'index du compte a update
                if current_account[keys]['account_number'] == account_number:
                    find_index = int(keys) - 1
            index += 1
        with open(self.path, "w", encoding="utf-8") as json_file:
            # Creation du nouveau dict {index: updated_account} param: updated account
            updated_index = f"{find_index + 1}"
            db_content["ACCOUNTS"][int(find_index)] = {updated_index: account_to_update}
            data_to_write = json.dumps(db_content, indent=self.indent)
            json_file.write(data_to_write)

    def open_db(self):
        """Open db, return all content"""
        with open(self.path, "r+", encoding="utf-8") as file:
            db_content = json.load(file)
        return db_content

    def reset_db(self):
        """Reset db.json from dbsave.json"""
        loading("Reset", 0.2)
        with open('DB_Json/dbsave.json', "r", encoding="utf-8") as file:
            db_save = json.load(file)
        with open(self.path, "w", encoding="utf-8") as file:
            data_to_write = json.dumps(db_save, indent=self.indent)
            file.write(data_to_write)
