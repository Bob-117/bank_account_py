"""Shell display tools"""
import os
import platform
import sys
import time
import json


def loading(param, waiting_time):
    """loading animation in shell"""
    display = param
    if isinstance(param, str):
        match param:
            case "Save":
                display = "Save"
            case "Reset":
                display = "Reset"

    loading_parts = ["[■□□□□□□□□□]", "[■■□□□□□□□□]", "[■■■□□□□□□□]", "[■■■■□□□□□□]", "[■■■■■□□□□□]",
                     "[■■■■■■□□□□]", "[■■■■■■■□□□]", "[■■■■■■■■□□]", "[■■■■■■■■■□]", "[■■■■■■■■■■]"]
    count = len(loading_parts)
    print(display)
    for i in range(count):
        time.sleep(waiting_time)
        sys.stdout.write("\r" + loading_parts[i % count])
        sys.stdout.flush()
    print("\n")


def tool_print_shell(account):
    """user need to print account details"""
    acc = account.get_all()
    # print(acc)

    for key in acc:
        separator = "*******************"

        print_shell_raw = str(key) + " " + str(acc[key])
        print_shell_array = [char for char in print_shell_raw]
        print_shell_array.append("\n")
        print_shell_array.append(separator)
        print_shell_array.append("\n")
        print_shell_array.append(separator)
        print_shell_array.append("\n")
        print_shell_array.insert(0, separator)
        print_shell_array.insert(1, "\n")

        count = len(print_shell_array)
        for i in range(count):
            time.sleep(0.1 % count)
            sys.stdout.write(print_shell_array[i % count])
            sys.stdout.flush()
    input("Press any key to continue...")


def printJson(data):
    """Print indent Json in shell"""
    # print(varname(data))
    print(json.dumps(data, indent=4, sort_keys=True))


def tool_clear_shell():
    """Clear the shell, switch (running os) case"""
    match platform.system():
        case "Windows":
            os.system('cls')
        case "Linux":
            os.system('clear')
        case "Darwin":
            os.system('clear')  # IOs
        case _:
            sys.exit('os error')
